window.onload = function() {
 updateRows();	
 $("select").bind('change', updateRows);
}

//returns an array of rows to populate the 
function updateRows()
{
	//get array data
	var data = getArrayData();
	//get filter from selection
	var filter = getFilter();
    //filter down data
    var filteredArray = getFilteredData(data, filter);
    //populate table values
    populateRows(filteredArray);
}

//populates view with result
function populateRows(array)
{

  var rows = "";	

  for(var i in array)
  {
    rows += "<tr>";

    rows+="<td><label>" + array[i][0] + "</label></td>";
    rows+="<td><label>" + array[i][1] + "</label></td>"
    rows+="<td><label>" + array[i][2] + "</label></td>"

    rows +="</tr>";
  }

  var html = $.parseHTML(rows);
  $('#output').html(html);
}

//applies filters to the data and spits back a filtered array
function getFilteredData(data, dataFilter)
{

  var filteredArray = data.filter(function(subArray)
  {
  	//if there are no filters, return everything
    if(jQuery.isEmptyObject(dataFilter))
    {
    	return true;
    }
    //there are filters applied
    else
    {   
    	//array of three values, if the filters aren't in all of the subarray values then move on
    	//else if the filters are in the subarray then add it into the filtered array
    	var boolArray = [];
    	for(var prop in dataFilter)
	    {
			boolArray.push(subArray.includes(dataFilter[prop])); 
	    }
	    if(!(boolArray.includes(false)))
	    {
	    	return true;
	    }
    }    	
  });

	return filteredArray;
}


//gets the filter from the DOM
function getFilter()
{
   var filter = {};

   var numberVal = document.getElementById("numberSelect").value;
   var colorVal = document.getElementById("colorSelect").value;
   var letterVal = document.getElementById("letterSelect").value;

   if(numberVal != 'all'){filter.number = numberVal;}
   if(colorVal != 'all'){filter.color = colorVal;}
   if(letterVal != 'all'){filter.letter = letterVal;}

   return filter;
}

//database
function getArrayData()
{
	var data = [
	['1','red','A'],['1','red','B'],['1','red','C'],
	['1','yellow','A'],['1','yellow','B'],['1','yellow','C'],
	['1','blue','A'],['1','blue','B'],['1','blue','C'],
	['2','red','A'],['2','red','B'],['2','red','C'],
	['2','yellow','A'],['2','yellow','B'],['2','yellow','C'],
	['2','blue','A'],['2','blue','B'],['2','blue','C'],
	['3','red','A'],['3','red','B'],['3','red','C'],
	['3','yellow','A'],['3','yellow','B'],['3','yellow','C'],
	['3','blue','A'],['3','blue','B'],['3','blue','C']
	];

	return data;
}


